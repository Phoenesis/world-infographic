import React from 'react';

const Card = ({country}) => { // {country} equivalent à props.country
    return (
        <li className={"card"}>
            <img src={country.flags.svg} alt={"Country " + country.translations.fra.common}/>
            <div className={"infos"}>
                <h2> {country.name.official} </h2>
                <h4> {country.capital} </h4>
               <p> Pop. {country.population.toLocaleString()}</p>
            </div>
        </li>
    );
};

export default Card;
