import React from 'react';
import Navigation from "../components/Navigation";
import Logo from "../components/Logo";

const About = () => {
    return (
        <div>
            <Logo/>
            <Navigation/>
            <h1> About </h1>
            <br/>
            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam eligendi error id incidunt magnam,
                nesciunt nihil omnis perferendis placeat quae quas quis saepe vitae! Et hic rem sed vitae! At, deserunt
                dignissimos doloremque eaque eveniet facilis fuga id, labore libero nam nulla omnis provident quam saepe
                sit soluta veritatis. Accusamus ad adipisci culpa dicta dolore dolores dolorum ea illo ipsam iure laboriosam
                magnam nemo nostrum optio provident quas ut, veniam voluptas! Accusantium dolor dolorem, ex impedit
                itaque iusto laudantium magni minus mollitia nemo odio officia optio pariatur possimus qui quis quod
                ratione saepe sequi similique sit tempore totam veritatis voluptas?
            </p>
        </div>
    );
};

export default About;
